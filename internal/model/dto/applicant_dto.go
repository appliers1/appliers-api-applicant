package dto

import (
	"appliers/api-applicant/pkg/constant"
	"time"
)

/* REQUEST */
type ApplicantRequestBody struct {
	UserID       uint            `json:"user_id"`
	FullName     string          `json:"fullname"`
	PlaceOfBirth string          `json:"place_of_birth"`
	DateOfBirth  string          `json:"date_of_birth"`
	Gender       constant.Gender `json:"gender" sql:"type:gender"`
	Photo        string          `json:"photo"`
	Phone        string          `json:"phone"`
	Address      string          `json:"address"`
	PostalCode   string          `json:"postal_code"`
}

type UpdateApplicantRequestBody struct {
	UserID       *uint            `json:"user_id"`
	FullName     *string          `json:"fullname"`
	PlaceOfBirth *string          `json:"place_of_birth"`
	DateOfBirth  *string          `json:"date_of_birth"`
	Gender       *constant.Gender `json:"gender" sql:"type:gender"`
	Photo        *string          `json:"photo"`
	Phone        *string          `json:"phone"`
	Address      *string          `json:"address"`
	PostalCode   *string          `json:"postal_code"`
}

/* RESPONSE */
type ApplicantResponse struct {
	ID           uint                 `json:"id"`
	UserID       uint                 `json:"user_id"`
	FullName     string               `json:"fullname"`
	PlaceOfBirth string               `json:"place_of_birth"`
	DateOfBirth  time.Time            `json:"date_of_birth"`
	Gender       constant.Gender      `json:"gender" sql:"type:gender"`
	Photo        string               `json:"photo"`
	Phone        string               `json:"phone"`
	Address      string               `json:"address"`
	PostalCode   string               `json:"postal_code"`
	Educations   []EducationResponse  `json:"educations"`
	Experiences  []ExperienceResponse `json:"experiences"`
}
