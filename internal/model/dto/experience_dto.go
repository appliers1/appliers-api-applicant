package dto

import "time"

/* REQUEST */
type ExperienceRequestBody struct {
	CompanyName string `json:"company_name"`
	Position    string `json:"position"`
	Jobdesk     string `json:"jobdesk"`
	StartDate   string `json:"start_date"`
	EndDate     string `json:"end_date"`
}

type UpdateExperienceRequestBody struct {
	CompanyName *string `json:"company_name"`
	Position    *string `json:"position"`
	Jobdesk     *string `json:"jobdesk"`
	StartDate   *string `json:"start_date"`
	EndDate     *string `json:"end_date"`
}

/* RESPONSE */
type ExperienceResponse struct {
	CompanyName string    `json:"company_name"`
	Position    string    `json:"position"`
	Jobdesk     string    `json:"jobdesk"`
	StartDate   time.Time `json:"start_date"`
	EndDate     time.Time `json:"end_date"`
}
