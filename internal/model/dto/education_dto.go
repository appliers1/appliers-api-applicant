package dto

import (
	"appliers/api-applicant/pkg/constant"
)

/* REQUEST */
type EducationRequestBody struct {
	DegreeType constant.DegreeType `json:"degree_type"`
	Name       string              `json:"name"`
}

type UpdateEducationRequestBody struct {
	DegreeType *constant.DegreeType `json:"degree_type"`
	Name       *string              `json:"name"`
}

/* RESPONSE */
type EducationResponse struct {
	ApplicantID uint   `json:"applicant_id"`
	DegreeType  string `json:"degree_type"`
	Name        string `json:"name"`
}
