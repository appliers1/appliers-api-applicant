package domain

import (
	"appliers/api-applicant/pkg/constant"
	"time"

	"gorm.io/gorm"
)

type Applicant struct {
	gorm.Model
	UserID       uint            `json:"user_id" gorm:"not null;uniqueIndex;"`
	FullName     string          `json:"fullname" gorm:"size:100;not null"`
	PlaceOfBirth string          `json:"place_of_birth" gorm:"size:100;not null"`
	DateOfBirth  time.Time       `json:"date_of_birth" gorm:"not null"`
	Gender       constant.Gender `json:"gender" sql:"type:gender" gorm:"type:enum('male', 'female');not null"`
	Photo        string          `json:"photo" gorm:"size:100"`
	Phone        string          `json:"phone" gorm:"size:14;"`
	Address      string          `json:"address" gorm:"size:500;"`
	PostalCode   string          `json:"postal_code" gorm:"size:9;"`
	Educations   []Education
	Experiences  []Experience
}

// BeforeCreate is a method for struct User
// gorm call this method before they execute query
func (u *Applicant) BeforeCreate(tx *gorm.DB) (err error) {
	u.CreatedAt = time.Now()
	return
}

// BeforeUpdate is a method for struct User
// gorm call this method before they execute query
func (u *Applicant) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}
