package domain

import (
	"time"

	"gorm.io/gorm"
)

type Experience struct {
	gorm.Model
	ApplicantID uint      `json:"applicant_id" gorm:"not null"`
	CompanyName string    `json:"company_name" gorm:"size:100;not null"`
	Position    string    `json:"position" gorm:"size:100;not null"`
	Jobdesk     string    `json:"jobdesk"`
	StartDate   time.Time `json:"start_date" gorm:"not null;"`
	EndDate     time.Time `json:"end_date"`
}

// BeforeCreate is a method for struct User
// gorm call this method before they execute query
func (u *Experience) BeforeCreate(tx *gorm.DB) (err error) {
	u.CreatedAt = time.Now()
	return
}

// BeforeUpdate is a method for struct User
// gorm call this method before they execute query
func (u *Experience) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}
