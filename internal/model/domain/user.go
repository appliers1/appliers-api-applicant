package domain

import (
	"appliers/api-applicant/pkg/constant"
)

type User struct {
	ID       uint                `json:"id"`
	Username string              `json:"username"`
	Email    string              `json:"email"`
	Role     constant.UserRole   `json:"role"`
	Status   constant.UserStatus `json:"status"`
}
