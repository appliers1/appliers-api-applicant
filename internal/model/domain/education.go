package domain

import (
	"appliers/api-applicant/pkg/constant"
	"time"

	"gorm.io/gorm"
)

type Education struct {
	gorm.Model
	ApplicantID uint                `json:"applicant_id" gorm:"not null"`
	DegreeType  constant.DegreeType `json:"degree_type" sql:"type:degree_type" gorm:"type:enum('1','2','3','4','5','6','7');not null"`
	Name        string              `json:"name" gorm:"size:100;not null"`
}

// BeforeCreate is a method for struct User
// gorm call this method before they execute query
func (u *Education) BeforeCreate(tx *gorm.DB) (err error) {
	u.CreatedAt = time.Now()
	return
}

// BeforeUpdate is a method for struct User
// gorm call this method before they execute query
func (u *Education) BeforeUpdate(tx *gorm.DB) (err error) {
	u.UpdatedAt = time.Now()
	return
}
