package education

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	"context"

	repository "appliers/api-applicant/internal/repository"
	res "appliers/api-applicant/pkg/util/response"
)

type EducationService interface {
	Create(ctx context.Context, payload *dto.EducationRequestBody, applicantId uint) (*dto.EducationResponse, error)
	Update(ctx context.Context, payload *dto.UpdateEducationRequestBody, applicantId uint, educationId uint) (*dto.EducationResponse, error)
	Delete(ctx context.Context, applicantId uint, educationId uint) error
	FindById(ctx context.Context, applicantId uint, educationId uint) (*dto.EducationResponse, error)
	Find(ctx context.Context, applicantId uint, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.EducationResponse], error)
}

type service struct {
	ApplicantRepository repository.ApplicantRepository
	EducationRepository repository.EducationRepository
}

func NewService(f *factory.ApplicantEducationFactory) EducationService {
	return &service{
		ApplicantRepository: f.ApplicantRepository,
		EducationRepository: f.EducationRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.EducationRequestBody, applicantId uint) (*dto.EducationResponse, error) {
	applicant, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if applicant == 1 {
			data, err := s.EducationRepository.Save(ctx, applicantId, 0, ToEducationDomain(payload))

			if err != nil {
				return nil, err
			} else {
				result := dto.EducationResponse{
					ApplicantID: data.ApplicantID,
					DegreeType:  data.DegreeType.String(),
					Name:        data.Name,
				}
				return &result, nil
			}
		} else {
			if applicant == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}

	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Update(ctx context.Context, payload *dto.UpdateEducationRequestBody, applicantId uint, educationId uint) (*dto.EducationResponse, error) {
	applicant, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if applicant == 1 {
			data, err := s.EducationRepository.Save(ctx, applicantId, 0, UpdateToEducationDomain(payload))

			if err != nil {
				return nil, err
			} else {
				result := dto.EducationResponse{
					ApplicantID: data.ApplicantID,
					DegreeType:  data.DegreeType.String(),
					Name:        data.Name,
				}
				return &result, nil
			}
		} else {
			if applicant == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)

}

func (s *service) Delete(ctx context.Context, applicantId uint, educationId uint) error {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			err := s.EducationRepository.Delete(ctx, educationId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, applicantId uint, educationId uint) (*dto.EducationResponse, error) {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			applicant, err := s.EducationRepository.FindById(ctx, educationId)
			if err == nil {
				userResponse := ToEducationResponse(*applicant)
				return &userResponse, nil
			} else {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, applicantId uint, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.EducationResponse], error) {

	users, info, err := s.EducationRepository.FindAll(ctx, applicantId, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var data []dto.EducationResponse

	for _, user := range users {
		data = append(data, ToEducationResponse(user))
	}

	result := new(dto.SearchGetResponse[dto.EducationResponse])
	result.Data = data
	result.PaginationInfo = *info

	return result, nil
}
