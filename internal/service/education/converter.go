package education

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
)

func ToEducationResponse(education domain.Education) dto.EducationResponse {
	return dto.EducationResponse{
		ApplicantID: education.ApplicantID,
		DegreeType:  education.DegreeType.String(),
		Name:        education.Name,
	}
}

func ToEducationDomain(req *dto.EducationRequestBody) domain.Education {
	education := domain.Education{
		DegreeType: req.DegreeType,
		Name:       req.Name,
	}
	return education
}

func UpdateToEducationDomain(req *dto.UpdateEducationRequestBody) domain.Education {
	education := domain.Education{}

	if req.DegreeType != nil {
		education.DegreeType = *req.DegreeType
	}

	if req.Name != nil {
		education.Name = *req.Name
	}
	return education
}
