package education

import (
	"appliers/api-applicant/internal/factory"
	_mockRepository "appliers/api-applicant/internal/mocks/repository"
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"appliers/api-applicant/internal/util/jwtpayload"
	"appliers/api-applicant/pkg/constant"
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var applicantRepository _mockRepository.ApplicantRepository
var educationRepository _mockRepository.EducationRepository
var experienceRepository _mockRepository.ExperienceRepository

var ctx context.Context
var educationService EducationService
var educationFactory = factory.ApplicantEducationFactory{
	ApplicantRepository: &applicantRepository,
	EducationRepository: &educationRepository,
}
var educationDomain domain.Education
var err error

func setup() {
	educationService = NewService(&educationFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	educationDomain = domain.Education{
		ApplicantID: 1,
		DegreeType:  constant.Master,
		Name:        "aa",
	}
}

func TestDelete(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	educationRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := educationService.Delete(ctx, uint(1), uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	educationRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := educationService.Delete(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	educationRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&educationDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		education, _ := educationService.FindById(context.Background(), uint(1), uint(1))
		assert.Equal(t, education.Name, educationDomain.Name)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	educationRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := educationService.FindById(context.Background(), uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	educationUpdate := domain.Education{
		ApplicantID: 1,
		DegreeType:  constant.Doctor,
		Name:        "g",
	}
	dto := dto.UpdateEducationRequestBody{
		DegreeType: &educationUpdate.DegreeType,
		Name:       &educationUpdate.Name,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	educationRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return(educationUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		education, _ := educationService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotEqual(t, education.Name, educationDomain.Name)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	educationUpdate := domain.Education{
		ApplicantID: 1,
		DegreeType:  constant.Doctor,
		Name:        "g",
	}
	dto := dto.UpdateEducationRequestBody{
		DegreeType: &educationUpdate.DegreeType,
		Name:       &educationUpdate.Name,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	educationRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {
		_, err := educationService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

// func TestCreate(t *testing.T) {
// 	setup()
// 	dto := dto.EducationRequestBody{
// 		DegreeType: educationDomain.DegreeType,
// 		Name:       educationDomain.Name,
// 	}
// 	applicantRepository.On("CountById",
// 		mock.Anything,
// 		mock.AnythingOfType("uint")).Return(1, nil).Once()
// 	educationRepository.On("Save",
// 		mock.Anything,
// 		mock.AnythingOfType("uint"),
// 		mock.AnythingOfType("uint"),
// 		mock.Anything).Return(educationDomain, nil).Once()
// 	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

// 		education, _ := educationService.Create(ctx, &dto, uint(1))
// 		assert.Equal(t, education.Name, educationDomain.Name)
// 	})
// }

func TestCreateFail(t *testing.T) {
	setup()
	dto := dto.EducationRequestBody{
		DegreeType: educationDomain.DegreeType,
		Name:       educationDomain.Name,
	}
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	educationRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		_, err := educationService.Create(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Education
	data = append(data, educationDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	educationRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		applicants, _ := educationService.Find(context.Background(), uint(1), &dto)
		assert.Equal(t, applicants.Data[0].Name, educationDomain.Name)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	educationRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		_, err := educationService.Find(context.Background(), uint(1), &dto)
		assert.NotNil(t, err)
	})
}
