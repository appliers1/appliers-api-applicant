package applicant

import (
	"appliers/api-applicant/internal/factory"
	_mockRepository "appliers/api-applicant/internal/mocks/repository"
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"appliers/api-applicant/internal/util/jwtpayload"
	"appliers/api-applicant/pkg/constant"
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var applicantRepository _mockRepository.ApplicantRepository
var educationRepository _mockRepository.EducationRepository
var experienceRepository _mockRepository.ExperienceRepository

var ctx context.Context
var applicantService ApplicantService
var applicantFactory = factory.ApplicantFactory{
	ApplicantRepository:  &applicantRepository,
	EducationRepository:  &educationRepository,
	ExperienceRepository: &experienceRepository,
}
var applicantDomain domain.Applicant
var err error

func setup() {
	applicantService = NewService(&applicantFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	applicantDomain = domain.Applicant{
		UserID:       4,
		FullName:     "aa",
		PlaceOfBirth: "kebumen",
		DateOfBirth:  time.Now(),
		Gender:       "male",
		Photo:        "dd",
		Phone:        "082",
		Address:      "sdd",
		PostalCode:   "456",
	}
}

func TestDelete(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicantRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := applicantService.Delete(ctx, uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicantRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := applicantService.Delete(ctx, uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicantRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&applicantDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		applicant, _ := applicantService.FindById(context.Background(), uint(1))
		assert.Equal(t, applicant.FullName, applicantDomain.FullName)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicantRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := applicantService.FindById(context.Background(), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	var dob = "2006-01-02 15:04:05"
	applicantUpdate := domain.Applicant{
		UserID:       4,
		FullName:     "bb",
		PlaceOfBirth: "kebumen",
		DateOfBirth:  time.Now(),
		Gender:       "male",
		Photo:        "dd",
		Phone:        "082",
		Address:      "sdd",
		PostalCode:   "456",
	}
	dto := dto.UpdateApplicantRequestBody{
		FullName:     &applicantUpdate.FullName,
		PlaceOfBirth: &applicantUpdate.PlaceOfBirth,
		DateOfBirth:  &dob,
		Gender:       &applicantDomain.Gender,
		Photo:        &applicantUpdate.Photo,
		Phone:        &applicantUpdate.Phone,
		Address:      &applicantUpdate.Address,
		PostalCode:   &applicantUpdate.PostalCode,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	applicantRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&applicantUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		applicant, _ := applicantService.Update(ctx, &dto, uint(1))
		assert.NotEqual(t, applicant.FullName, applicantDomain.FullName)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	var dob = "2006-01-02 15:04:05"
	applicantUpdate := domain.Applicant{
		UserID:       4,
		FullName:     "bb",
		PlaceOfBirth: "kebumen",
		DateOfBirth:  time.Now(),
		Gender:       "male",
		Photo:        "dd",
		Phone:        "082",
		Address:      "sdd",
		PostalCode:   "456",
	}
	dto := dto.UpdateApplicantRequestBody{
		FullName:     &applicantUpdate.FullName,
		PlaceOfBirth: &applicantUpdate.PlaceOfBirth,
		DateOfBirth:  &dob,
		Gender:       &applicantDomain.Gender,
		Photo:        &applicantUpdate.Photo,
		Phone:        &applicantUpdate.Phone,
		Address:      &applicantUpdate.Address,
		PostalCode:   &applicantUpdate.PostalCode,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	applicantRepository.On("Update",
		mock.Anything,
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {
		_, err := applicantService.Update(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestCreate(t *testing.T) {
	setup()
	var dob = "2006-01-02 15:04:05"
	dto := dto.ApplicantRequestBody{
		UserID:       applicantDomain.UserID,
		FullName:     applicantDomain.FullName,
		PlaceOfBirth: applicantDomain.PlaceOfBirth,
		DateOfBirth:  dob,
		Gender:       applicantDomain.Gender,
		Photo:        applicantDomain.Photo,
		Phone:        applicantDomain.Phone,
		Address:      applicantDomain.Address,
		PostalCode:   applicantDomain.PostalCode,
	}
	applicantRepository.On("Save",
		mock.Anything,
		mock.Anything).Return(applicantDomain, nil).Once()
	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {

		applicant, _ := applicantService.Create(ctx, &dto)
		assert.Equal(t, applicant.FullName, applicantDomain.FullName)
	})
}

func TestCreateFail(t *testing.T) {
	setup()
	var dob = "2006-01-02 15:04:05"
	dto := dto.ApplicantRequestBody{
		UserID:       applicantDomain.UserID,
		FullName:     applicantDomain.FullName,
		PlaceOfBirth: applicantDomain.PlaceOfBirth,
		DateOfBirth:  dob,
		Gender:       applicantDomain.Gender,
		Photo:        applicantDomain.Photo,
		Phone:        applicantDomain.Phone,
		Address:      applicantDomain.Address,
		PostalCode:   applicantDomain.PostalCode,
	}
	applicantRepository.On("Save",
		mock.Anything,
		mock.Anything).Return((domain.Applicant{}), err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		_, err := applicantService.Create(ctx, &dto)
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Applicant
	data = append(data, applicantDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	applicantRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		applicants, _ := applicantService.Find(context.Background(), &dto)
		assert.Equal(t, applicants.Data[0].FullName, applicantDomain.FullName)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	applicantRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		_, err := applicantService.Find(context.Background(), &dto)
		assert.NotNil(t, err)
	})
}
