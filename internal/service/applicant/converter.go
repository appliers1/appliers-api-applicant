package applicant

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"appliers/api-applicant/internal/service/education"
	"appliers/api-applicant/internal/service/experience"
	"time"
)

func ToApplicantResponse(applicant domain.Applicant) dto.ApplicantResponse {

	var educations = []dto.EducationResponse{}
	for _, e := range applicant.Educations {
		educations = append(educations, education.ToEducationResponse(e))
	}

	var experiences = []dto.ExperienceResponse{}
	for _, exp := range applicant.Experiences {
		experiences = append(experiences, experience.ToExperienceResponse(exp))
	}

	return dto.ApplicantResponse{
		ID:           applicant.ID,
		UserID:       applicant.UserID,
		FullName:     applicant.FullName,
		PlaceOfBirth: applicant.PlaceOfBirth,
		DateOfBirth:  applicant.DateOfBirth,
		Gender:       applicant.Gender,
		Photo:        applicant.Photo,
		Phone:        applicant.Phone,
		Address:      applicant.Address,
		PostalCode:   applicant.PostalCode,
		Educations:   educations,
		Experiences:  experiences,
	}
}

func ToApplicantDomain(req *dto.ApplicantRequestBody) domain.Applicant {
	var dateFormat = "2006-01-02"

	dob, err := time.Parse(dateFormat, req.DateOfBirth)

	if err != nil {
		dob = time.Time{}
	}

	applicant := domain.Applicant{
		UserID:       req.UserID,
		FullName:     req.FullName,
		PlaceOfBirth: req.PlaceOfBirth,
		DateOfBirth:  dob,
		Gender:       req.Gender,
		Photo:        req.Photo,
		Phone:        req.Phone,
		Address:      req.Address,
		PostalCode:   req.PostalCode,
	}

	return applicant
}

func UpdateToApplicantDomain(req *dto.UpdateApplicantRequestBody) domain.Applicant {
	applicant := domain.Applicant{}

	if req.UserID != nil {
		applicant.UserID = *req.UserID
	}

	if req.FullName != nil {
		applicant.FullName = *req.FullName
	}

	if req.DateOfBirth != nil {
		var dateFormat = "2006-01-02"
		dob, err := time.Parse(dateFormat, *req.DateOfBirth)
		if err == nil {
			applicant.DateOfBirth = dob
		}
	}

	if req.PlaceOfBirth != nil {
		applicant.PlaceOfBirth = *req.PlaceOfBirth
	}

	if req.Gender != nil {
		applicant.Gender = *req.Gender
	}

	if req.Phone != nil {
		applicant.Phone = *req.Phone
	}

	if req.Photo != nil {
		applicant.Photo = *req.Photo
	}

	if req.PostalCode != nil {
		applicant.PostalCode = *req.PostalCode
	}

	return applicant
}
