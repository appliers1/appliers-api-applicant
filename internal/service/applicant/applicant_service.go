package applicant

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	"context"
	"fmt"

	repository "appliers/api-applicant/internal/repository"
	res "appliers/api-applicant/pkg/util/response"
)

type ApplicantService interface {
	Create(ctx context.Context, payload *dto.ApplicantRequestBody) (*dto.ApplicantResponse, error)
	Update(ctx context.Context, payload *dto.UpdateApplicantRequestBody, applicantId uint) (*dto.ApplicantResponse, error)
	Delete(ctx context.Context, applicantId uint) error
	FindById(ctx context.Context, applicantId uint) (*dto.ApplicantResponse, error)
	Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ApplicantResponse], error)
	FindByUserId(ctx context.Context, userId uint) (*dto.ApplicantResponse, error)
}

type service struct {
	ApplicantRepository repository.ApplicantRepository
}

func NewService(f *factory.ApplicantFactory) ApplicantService {
	return &service{
		ApplicantRepository: f.ApplicantRepository,
	}
}

func (s *service) Create(ctx context.Context, payload *dto.ApplicantRequestBody) (*dto.ApplicantResponse, error) {
	data, err := s.ApplicantRepository.Save(ctx, ToApplicantDomain(payload))

	if err != nil {
		return nil, err
	} else {
		result := ToApplicantResponse(data)
		return &result, nil
	}
}

func (s *service) Update(ctx context.Context, payload *dto.UpdateApplicantRequestBody, applicantId uint) (*dto.ApplicantResponse, error) {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)

	if err == nil {
		if count == 1 {

			data, err := s.ApplicantRepository.Update(ctx, UpdateToApplicantDomain(payload), applicantId)
			fmt.Println(err)
			if err == nil {
				data.ID = applicantId
				applicantResponse := ToApplicantResponse(*data)
				return &applicantResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)

}

func (s *service) Delete(ctx context.Context, applicantId uint) error {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			err := s.ApplicantRepository.Delete(ctx, applicantId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, applicantId uint) (*dto.ApplicantResponse, error) {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			applicant, err := s.ApplicantRepository.FindById(ctx, applicantId)
			if err == nil {
				applicantResponse := ToApplicantResponse(*applicant)
				return &applicantResponse, nil
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ApplicantResponse], error) {

	applicants, info, err := s.ApplicantRepository.FindAll(ctx, payload, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var data []dto.ApplicantResponse

	for _, applicant := range applicants {
		data = append(data, ToApplicantResponse(applicant))
	}

	result := new(dto.SearchGetResponse[dto.ApplicantResponse])
	result.Data = data
	result.PaginationInfo = *info

	return result, nil
}

func (s *service) FindByUserId(ctx context.Context, userId uint) (*dto.ApplicantResponse, error) {
	applicant, err := s.ApplicantRepository.FindByUserId(ctx, userId)
	if err != nil {
		return nil, err
	}
	applicantResponse := ToApplicantResponse(*applicant)
	return &applicantResponse, nil
}
