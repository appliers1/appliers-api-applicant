package experience

import (
	"appliers/api-applicant/internal/factory"
	_mockRepository "appliers/api-applicant/internal/mocks/repository"
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"appliers/api-applicant/internal/util/jwtpayload"
	"appliers/api-applicant/pkg/constant"
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

var applicantRepository _mockRepository.ApplicantRepository
var experienceRepository _mockRepository.ExperienceRepository

var ctx context.Context
var experienceService ExperienceService
var experienceFactory = factory.ApplicantExperienceFactory{
	ApplicantRepository:  &applicantRepository,
	ExperienceRepository: &experienceRepository,
}
var experienceDomain domain.Experience
var err error

func setup() {
	experienceService = NewService(&experienceFactory)
	payload := jwtpayload.JWTPayload{
		UserID:        1,
		Role:          constant.ROLE_COMPANY,
		InstitutionId: 1,
	}
	ctx = context.Background()
	ctx = context.WithValue(ctx, constant.CONTEXT_JWT_PAYLOAD_KEY, &payload)
	err = errors.New("mock")
	experienceDomain = domain.Experience{
		ApplicantID: 1,
		CompanyName: "Dataon",
		Position:    "Staff",
		Jobdesk:     "Programmer",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
	}
}

func TestDelete(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	experienceRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 1 | Valid Delete", func(t *testing.T) {
		err := experienceService.Delete(ctx, uint(1), uint(1))
		assert.Nil(t, err)
	})
}

func TestDeleteFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicantRepository.On("Delete",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 2 | Invalid Delete", func(t *testing.T) {
		err := experienceService.Delete(ctx, uint(1), uint(1))
		assert.NotNil(t, err)
	})

}

func TestFindById(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	experienceRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(&experienceDomain, nil).Once()

	t.Run("Test Case 5 | Valid FindById", func(t *testing.T) {
		applicant, _ := experienceService.FindById(context.Background(), uint(1), uint(1))
		assert.Equal(t, applicant.CompanyName, experienceDomain.CompanyName)
	})
}

func TestFindByIdFail(t *testing.T) {
	setup()
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, nil).Once()
	applicantRepository.On("FindById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(nil, nil).Once()

	t.Run("Test Case 6 | Invalid FindById", func(t *testing.T) {
		_, err := experienceService.FindById(context.Background(), uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

func TestUpdate(t *testing.T) {
	setup()
	var date = "2006-01-02 15:04:05"
	experienceUpdate := domain.Experience{
		ApplicantID: 1,
		CompanyName: "Dataon 1",
		Position:    "Staff",
		Jobdesk:     "Programmer",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
	}
	dto := dto.UpdateExperienceRequestBody{
		CompanyName: &experienceUpdate.CompanyName,
		Position:    &experienceUpdate.Position,
		Jobdesk:     &experienceUpdate.Jobdesk,
		StartDate:   &date,
		EndDate:     &date,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(1, nil).Once()
	experienceRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return(experienceUpdate, nil).Once()

	t.Run("Test Case 3 | Valid Update", func(t *testing.T) {

		experience, _ := experienceService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotEqual(t, experience.CompanyName, experienceDomain.CompanyName)
	})
}

func TestUpdateFail(t *testing.T) {
	setup()
	var date = "2006-01-02 15:04:05"
	experienceUpdate := domain.Experience{
		ApplicantID: 1,
		CompanyName: "Dataon 1",
		Position:    "Staff",
		Jobdesk:     "Programmer",
		StartDate:   time.Now(),
		EndDate:     time.Now(),
	}
	dto := dto.UpdateExperienceRequestBody{
		CompanyName: &experienceUpdate.CompanyName,
		Position:    &experienceUpdate.Position,
		Jobdesk:     &experienceUpdate.Jobdesk,
		StartDate:   &date,
		EndDate:     &date,
	}

	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	experienceRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return(nil, err).Once()

	t.Run("Test Case 3 | Invalid Update", func(t *testing.T) {
		_, err := experienceService.Update(ctx, &dto, uint(1), uint(1))
		assert.NotNil(t, err)
	})
}

// func TestCreate(t *testing.T) {
// 	setup()
// 	var date = "2006-01-02 15:04:05"
// 	dto := dto.ExperienceRequestBody{
// 		CompanyName: experienceDomain.CompanyName,
// 		Position:    experienceDomain.Position,
// 		Jobdesk:     experienceDomain.Jobdesk,
// 		StartDate:   date,
// 		EndDate:     date,
// 	}
// 	applicantRepository.On("CountById",
// 		mock.Anything,
// 		mock.AnythingOfType("uint")).Return(1, nil).Once()
// 	experienceRepository.On("Save",
// 		mock.Anything,
// 		mock.AnythingOfType("uint"),
// 		mock.AnythingOfType("uint"),
// 		mock.Anything).Return(experienceDomain, nil).Once()

// 	t.Run("Test Case 3 | Valid Create ", func(t *testing.T) {

// 		experience, _ := experienceService.Create(ctx, &dto, uint(1))
// 		assert.Equal(t, experience.CompanyName, experienceDomain.CompanyName)
// 	})
// }

func TestCreateFail(t *testing.T) {
	setup()
	var date = "2006-01-02 15:04:05"
	dto := dto.ExperienceRequestBody{
		CompanyName: experienceDomain.CompanyName,
		Position:    experienceDomain.Position,
		Jobdesk:     experienceDomain.Jobdesk,
		StartDate:   date,
		EndDate:     date,
	}
	applicantRepository.On("CountById",
		mock.Anything,
		mock.AnythingOfType("uint")).Return(0, err).Once()
	experienceRepository.On("Save",
		mock.Anything,
		mock.AnythingOfType("uint"),
		mock.AnythingOfType("uint"),
		mock.Anything).Return((domain.Experience{}), err).Once()

	t.Run("Test Case 3 | Valid Create", func(t *testing.T) {
		_, err := experienceService.Create(ctx, &dto, uint(1))
		assert.NotNil(t, err)
	})
}

func TestFind(t *testing.T) {
	setup()
	var data []domain.Experience
	data = append(data, experienceDomain)
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	experienceRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(data, dto.CheckInfoPagination(&pagination, 10), nil).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		applicants, _ := experienceService.Find(context.Background(), uint(1), &dto)
		assert.Equal(t, applicants.Data[0].CompanyName, experienceDomain.CompanyName)
	})
}

func TestFindFail(t *testing.T) {
	setup()
	page := 1
	limit := 10
	pagination := dto.Pagination{
		Page:     &page,
		PageSize: &limit,
	}
	experienceRepository.On("FindAll",
		mock.Anything,
		mock.Anything,
		mock.Anything).Return(nil, nil, err).Once()

	t.Run("Test Case 5 | Valid Find", func(t *testing.T) {

		dto := dto.SearchGetRequest{
			Pagination: pagination,
			Search:     "aa",
		}
		_, err := experienceService.Find(context.Background(), uint(1), &dto)
		assert.NotNil(t, err)
	})
}
