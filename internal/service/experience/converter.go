package experience

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"time"
)

func ToExperienceResponse(experience domain.Experience) dto.ExperienceResponse {
	return dto.ExperienceResponse{
		CompanyName: experience.CompanyName,
		Position:    experience.Position,
		Jobdesk:     experience.Jobdesk,
		StartDate:   experience.StartDate,
		EndDate:     experience.EndDate,
	}
}

func ToExperienceDomain(req *dto.ExperienceRequestBody) domain.Experience {
	var dateFormat = "2006-01-02 15:04:05"
	startDate, err := time.Parse(dateFormat, req.StartDate)

	if err != nil {
		startDate = time.Now()
	}

	endDate, err := time.Parse(dateFormat, req.EndDate)

	if err != nil {
		endDate = time.Now()
	}

	experience := domain.Experience{
		CompanyName: req.CompanyName,
		Position:    req.Position,
		Jobdesk:     req.Jobdesk,
		StartDate:   startDate,
		EndDate:     endDate,
	}
	return experience
}

func UpdateToExperienceDomain(req *dto.UpdateExperienceRequestBody) domain.Experience {
	experience := domain.Experience{}
	var dateFormat = "2006-01-02 15:04:05"

	startDate, err := time.Parse(dateFormat, *req.StartDate)

	if err != nil {
		startDate = time.Now()
	}

	endDate, err := time.Parse(dateFormat, *req.EndDate)

	if err != nil {
		endDate = time.Now()
	}

	if req.CompanyName != nil {
		experience.CompanyName = *req.CompanyName
	}

	if req.Position != nil {
		experience.Position = *req.Position
	}

	if req.Jobdesk != nil {
		experience.Jobdesk = *req.Jobdesk
	}

	if req.StartDate != nil {
		experience.StartDate = startDate
	}

	if req.EndDate != nil {
		experience.EndDate = endDate
	}

	return experience
}
