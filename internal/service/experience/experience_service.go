package experience

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	"context"
	"fmt"

	repository "appliers/api-applicant/internal/repository"
	res "appliers/api-applicant/pkg/util/response"
)

type ExperienceService interface {
	Create(ctx context.Context, request *dto.ExperienceRequestBody, applicantId uint) (*dto.ExperienceResponse, error)
	Update(ctx context.Context, request *dto.UpdateExperienceRequestBody, applicantId uint, experienceId uint) (*dto.ExperienceResponse, error)
	Delete(ctx context.Context, applicantId uint, experienceId uint) error
	FindById(ctx context.Context, applicantId uint, experienceId uint) (*dto.ExperienceResponse, error)
	Find(ctx context.Context, applicantId uint, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ExperienceResponse], error)
}

type service struct {
	ApplicantRepository  repository.ApplicantRepository
	ExperienceRepository repository.ExperienceRepository
}

func NewService(f *factory.ApplicantExperienceFactory) ExperienceService {
	return &service{
		ApplicantRepository:  f.ApplicantRepository,
		ExperienceRepository: f.ExperienceRepository,
	}
}
func (s *service) Create(ctx context.Context, payload *dto.ExperienceRequestBody, applicantId uint) (*dto.ExperienceResponse, error) {
	applicant, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if applicant == 1 {
			data, err := s.ExperienceRepository.Save(ctx, applicantId, 0, ToExperienceDomain(payload))
			fmt.Println(data)
			if err != nil {
				return nil, err
			} else {
				result := ToExperienceResponse(data)
				return &result, nil
			}
		} else {
			if applicant == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}

	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)

}

func (s *service) Update(ctx context.Context, request *dto.UpdateExperienceRequestBody, applicantId uint, experienceId uint) (*dto.ExperienceResponse, error) {
	applicant, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if applicant == 1 {
			data, err := s.ExperienceRepository.Save(ctx, applicantId, experienceId, UpdateToExperienceDomain(request))

			if err == nil {
				experienceResponse := ToExperienceResponse(data)
				return &experienceResponse, nil
			}

		} else {
			if applicant == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)

}

func (s *service) Delete(ctx context.Context, applicantId uint, experienceId uint) error {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			err := s.ExperienceRepository.Delete(ctx, experienceId)
			if err == nil {
				return nil
			}
		} else {
			if count == 0 {
				return res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) FindById(ctx context.Context, applicantId uint, experienceId uint) (*dto.ExperienceResponse, error) {
	count, err := s.ApplicantRepository.CountById(ctx, applicantId)
	if err == nil {
		if count == 1 {
			applicant, err := s.ExperienceRepository.FindById(ctx, experienceId)
			if err == nil {
				userResponse := ToExperienceResponse(*applicant)
				return &userResponse, nil
			} else {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		} else {
			if count == 0 {
				return nil, res.ErrorBuilder(&res.ErrorConstant.NotFound, err)
			}
		}
	}
	return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
}

func (s *service) Find(ctx context.Context, applicantId uint, payload *dto.SearchGetRequest) (*dto.SearchGetResponse[dto.ExperienceResponse], error) {

	users, info, err := s.ExperienceRepository.FindAll(ctx, applicantId, &payload.Pagination)
	if err != nil {
		return nil, res.ErrorBuilder(&res.ErrorConstant.InternalServerError, err)
	}

	var data []dto.ExperienceResponse

	for _, user := range users {
		data = append(data, ToExperienceResponse(user))
	}

	result := new(dto.SearchGetResponse[dto.ExperienceResponse])
	result.Data = data
	result.PaginationInfo = *info

	return result, nil
}
