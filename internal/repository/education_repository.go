package repository

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"context"

	"gorm.io/gorm"
)

type EducationRepository interface {
	Save(ctx context.Context, applicantId uint, educationId uint, education domain.Education) (domain.Education, error)
	// Update(ctx context.Context, education domain.Education, applicantId uint, educationId uint) (*domain.Education, error)
	Delete(ctx context.Context, educationId uint) error
	FindById(ctx context.Context, educationId uint) (*domain.Education, error)
	FindAll(ctx context.Context, applicantId uint, p *dto.Pagination) ([]domain.Education, *dto.PaginationInfo, error)
	CountById(ctx context.Context, educationId uint) (int, error)
}

type EducationRepositoryImplSql struct {
	DB *gorm.DB
}

func NewEducationRepositoryImplSql(db *gorm.DB) EducationRepository {
	return &EducationRepositoryImplSql{
		DB: db,
	}
}

func (repository *EducationRepositoryImplSql) Save(ctx context.Context, applicantId uint, educationId uint, education domain.Education) (domain.Education, error) {
	education.ApplicantID = applicantId
	education.ID = educationId
	err := repository.DB.WithContext(ctx).Save(&education).Error
	return education, err
}

// func (repository *EducationRepositoryImplSql) Update(ctx context.Context, education domain.Education, applicantId uint, educationId uint) (*domain.Education, error) {
// 	education.ApplicantID = applicantId
// 	err := repository.DB.WithContext(ctx).Model(&education).Where("id = ?", educationId).Updates(&education).Error
// 	updatedEducation := domain.Education{}
// 	repository.DB.WithContext(ctx).First(&updatedEducation, educationId)
// 	return &updatedEducation, err
// }

func (repository *EducationRepositoryImplSql) Delete(ctx context.Context, educationId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&domain.Education{}, educationId).Error
	if err == nil {
		education := domain.Education{}
		err = repository.DB.WithContext(ctx).Unscoped().Model(&education).Where("id = ?", educationId).Updates(&education).Error
	}
	return err
}

func (repository *EducationRepositoryImplSql) FindById(ctx context.Context, educationId uint) (*domain.Education, error) {
	var education = domain.Education{}
	err := repository.DB.WithContext(ctx).First(&education, educationId).Error
	if err == nil {
		return &education, nil
	} else {
		return nil, err
	}
}

func (repository *EducationRepositoryImplSql) FindAll(ctx context.Context, applicantId uint, p *dto.Pagination) ([]domain.Education, *dto.PaginationInfo, error) {
	var educations []domain.Education
	var count int64

	query := repository.DB.WithContext(ctx).Model(&domain.Education{}).Where("applicant_id = ? ", applicantId)

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&educations).Error

	return educations, dto.CheckInfoPagination(p, count), err
}

func (repository *EducationRepositoryImplSql) CountById(ctx context.Context, educationId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&domain.Education{}).Where("id = ?", educationId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
