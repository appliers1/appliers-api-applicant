package repository

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"context"

	"gorm.io/gorm"
)

type ExperienceRepository interface {
	Save(ctx context.Context, applicantId uint, experienceId uint, experience domain.Experience) (domain.Experience, error)
	Delete(ctx context.Context, experienceId uint) error
	FindById(ctx context.Context, experienceId uint) (*domain.Experience, error)
	FindAll(ctx context.Context, applicantId uint, p *dto.Pagination) ([]domain.Experience, *dto.PaginationInfo, error)
	CountById(ctx context.Context, experienceId uint) (int, error)
}

type ExperienceRepositoryImplSql struct {
	DB *gorm.DB
}

func NewExperienceRepositoryImplSql(db *gorm.DB) ExperienceRepository {
	return &ExperienceRepositoryImplSql{
		DB: db,
	}
}

func (repository *ExperienceRepositoryImplSql) Save(ctx context.Context, applicantId uint, experienceId uint, experience domain.Experience) (domain.Experience, error) {
	experience.ApplicantID = applicantId
	experience.ID = experienceId
	err := repository.DB.WithContext(ctx).Save(&experience).Error
	return experience, err
}

func (repository *ExperienceRepositoryImplSql) Delete(ctx context.Context, experienceId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&domain.Experience{}, experienceId).Error
	if err == nil {
		experience := domain.Experience{}
		err = repository.DB.WithContext(ctx).Unscoped().Model(&experience).Where("id = ?", experienceId).Updates(&experience).Error
	}
	return err
}

func (repository *ExperienceRepositoryImplSql) FindById(ctx context.Context, experienceId uint) (*domain.Experience, error) {
	var experience = domain.Experience{}
	err := repository.DB.WithContext(ctx).First(&experience, experienceId).Error
	if err == nil {
		return &experience, nil
	} else {
		return nil, err
	}
}

func (repository *ExperienceRepositoryImplSql) FindAll(ctx context.Context, applicantId uint, p *dto.Pagination) ([]domain.Experience, *dto.PaginationInfo, error) {
	var experiences []domain.Experience
	var count int64

	query := repository.DB.WithContext(ctx).Model(&domain.Experience{}).Where("applicant_id = ? ", applicantId)

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Limit(limit).Offset(offset).Find(&experiences).Error

	return experiences, dto.CheckInfoPagination(p, count), err
}

func (repository *ExperienceRepositoryImplSql) CountById(ctx context.Context, experienceId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&domain.Experience{}).Where("id = ?", experienceId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}
