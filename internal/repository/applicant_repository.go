package repository

import (
	"appliers/api-applicant/internal/model/domain"
	"appliers/api-applicant/internal/model/dto"
	"context"
	"strings"

	"gorm.io/gorm"
)

type ApplicantRepository interface {
	Save(ctx context.Context, applicant domain.Applicant) (domain.Applicant, error)
	Update(ctx context.Context, applicant domain.Applicant, applicantId uint) (*domain.Applicant, error)
	Delete(ctx context.Context, applicantId uint) error
	FindById(ctx context.Context, applicantId uint) (*domain.Applicant, error)
	FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]domain.Applicant, *dto.PaginationInfo, error)
	CountById(ctx context.Context, applicantId uint) (int, error)
	FindByUserId(ctx context.Context, userId uint) (*domain.Applicant, error)
}

type ApplicantRepositoryImplSql struct {
	DB *gorm.DB
}

func NewApplicantRepositoryImplSql(db *gorm.DB) ApplicantRepository {
	return &ApplicantRepositoryImplSql{
		DB: db,
	}
}

func (repository *ApplicantRepositoryImplSql) Save(ctx context.Context, applicant domain.Applicant) (domain.Applicant, error) {
	err := repository.DB.WithContext(ctx).Save(&applicant).Error
	return applicant, err
}

func (repository *ApplicantRepositoryImplSql) Update(ctx context.Context, applicant domain.Applicant, applicantId uint) (*domain.Applicant, error) {
	err := repository.DB.WithContext(ctx).Model(&applicant).Where("id = ?", applicantId).Updates(&applicant).Error
	updatedApplicant := domain.Applicant{}
	repository.DB.WithContext(ctx).First(&updatedApplicant, applicantId)
	return &updatedApplicant, err
}

func (repository *ApplicantRepositoryImplSql) Delete(ctx context.Context, applicantId uint) error {
	err := repository.DB.WithContext(ctx).Delete(&domain.Applicant{}, applicantId).Error
	if err == nil {
		applicant := domain.Applicant{}
		err = repository.DB.WithContext(ctx).Unscoped().Model(&applicant).Where("id = ?", applicantId).Updates(&applicant).Error
	}
	return err
}

func (repository *ApplicantRepositoryImplSql) FindById(ctx context.Context, applicantId uint) (*domain.Applicant, error) {
	var applicant = domain.Applicant{}
	err := repository.DB.WithContext(ctx).Preload("Educations").Preload("Experiences").First(&applicant, applicantId).Error
	if err == nil {
		return &applicant, nil
	} else {
		return nil, err
	}
}

func (repository *ApplicantRepositoryImplSql) FindAll(ctx context.Context, payload *dto.SearchGetRequest, p *dto.Pagination) ([]domain.Applicant, *dto.PaginationInfo, error) {
	var applicants []domain.Applicant
	var count int64

	query := repository.DB.WithContext(ctx).Model(&domain.Applicant{})

	if payload.Search != "" {
		search := "%" + strings.ToLower(payload.Search) + "%"
		query = query.Where("lower(fullname) LIKE ?", search)
	}

	countQuery := query
	if err := countQuery.Count(&count).Error; err != nil {
		return nil, nil, err
	}

	limit, offset := dto.GetLimitOffset(p)

	err := query.Preload("Educations").Preload("Experiences").Limit(limit).Offset(offset).Find(&applicants).Error

	return applicants, dto.CheckInfoPagination(p, count), err
}

func (repository *ApplicantRepositoryImplSql) CountById(ctx context.Context, applicantId uint) (int, error) {
	var count int64
	err := repository.DB.WithContext(ctx).Model(&domain.Applicant{}).Where("id = ?", applicantId).Count(&count).Error
	if err != nil {
		return -1, err
	}
	return int(count), nil
}

func (repository *ApplicantRepositoryImplSql) FindByUserId(ctx context.Context, userId uint) (*domain.Applicant, error) {
	var applicant = domain.Applicant{}
	err := repository.DB.WithContext(ctx).Where("user_id = ? ", userId).First(&applicant).Error
	if err != nil {
		return nil, err
	}
	return &applicant, nil
}
