package http

import (
	"appliers/api-applicant/internal/factory"
	c "appliers/api-applicant/internal/http/controller"
	"appliers/api-applicant/internal/http/router"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func Init(e *echo.Echo, db *gorm.DB) {
	router.ApplicantRouter(e, c.NewApplicantController(factory.NewApplicantFactory(db)))
	router.ApplicantEducationRouter(e, c.NewApplicantEducationController(factory.NewEducationFactory(db)))
	router.ApplicantExperienceRouter(e, c.NewApplicantExperienceController(factory.NewApplicantExperienceFactory(db)))
}
