package controller

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	service "appliers/api-applicant/internal/service/education"
	"appliers/api-applicant/internal/util/compare"
	"strconv"

	res "appliers/api-applicant/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type ApplicantEducationController struct {
	EducationService service.EducationService
}

func NewApplicantEducationController(f *factory.ApplicantEducationFactory) *ApplicantEducationController {
	return &ApplicantEducationController{
		EducationService: service.NewService(f),
	}
}

func (controller *ApplicantEducationController) Create(c echo.Context) error {
	payload := new(dto.EducationRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_applicant, errId := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	user, err := controller.EducationService.Create(c.Request().Context(), payload, uint(id_applicant))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantEducationController) Update(c echo.Context) error {
	payload := new(dto.UpdateEducationRequestBody)
	err := c.Bind(payload)
	id_applicant, errId := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	id_education, errId := strconv.ParseUint(c.Param("id_education"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	user, err := controller.EducationService.Update(c.Request().Context(), payload, uint(id_applicant), uint(id_education))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantEducationController) Delete(c echo.Context) error {
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_education, err := strconv.ParseUint(c.Param("id_education"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	err = controller.EducationService.Delete(c.Request().Context(), uint(id_applicant), uint(id_education))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *ApplicantEducationController) FindById(c echo.Context) error {
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_education, err := strconv.ParseUint(c.Param("id_education"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	user, err := controller.EducationService.FindById(c.Request().Context(), uint(id_applicant), uint(id_education))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantEducationController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	result, err := controller.EducationService.Find(c.Request().Context(), uint(id_applicant), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(result.Data).Send(c)
}
