package controller

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	service "appliers/api-applicant/internal/service/experience"
	"appliers/api-applicant/internal/util/compare"
	"strconv"

	res "appliers/api-applicant/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type ApplicantExperienceController struct {
	ExperienceService service.ExperienceService
}

func NewApplicantExperienceController(f *factory.ApplicantExperienceFactory) *ApplicantExperienceController {
	return &ApplicantExperienceController{
		ExperienceService: service.NewService(f),
	}
}

func (controller *ApplicantExperienceController) Create(c echo.Context) error {
	payload := new(dto.ExperienceRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_applicant, errId := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, errId).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	user, err := controller.ExperienceService.Create(c.Request().Context(), payload, uint(id_applicant))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantExperienceController) Update(c echo.Context) error {
	payload := new(dto.UpdateExperienceRequestBody)
	err := c.Bind(payload)
	id_applicant, errId := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	id_experience, errId := strconv.ParseUint(c.Param("id_experience"), 10, 32)
	if errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	user, err := controller.ExperienceService.Update(c.Request().Context(), payload, uint(id_applicant), uint(id_experience))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantExperienceController) Delete(c echo.Context) error {
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_experience, err := strconv.ParseUint(c.Param("id_experience"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	err = controller.ExperienceService.Delete(c.Request().Context(), uint(id_applicant), uint(id_experience))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *ApplicantExperienceController) FindById(c echo.Context) error {
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	id_experience, err := strconv.ParseUint(c.Param("id_experience"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	user, err := controller.ExperienceService.FindById(c.Request().Context(), uint(id_applicant), uint(id_experience))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(user).Send(c)
}

func (controller *ApplicantExperienceController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}
	id_applicant, err := strconv.ParseUint(c.Param("id_applicant"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id_applicant)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}
	result, err := controller.ExperienceService.Find(c.Request().Context(), uint(id_applicant), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(result.Data).Send(c)
}
