package controller

import (
	"appliers/api-applicant/internal/factory"
	"appliers/api-applicant/internal/model/dto"
	service "appliers/api-applicant/internal/service/applicant"
	"appliers/api-applicant/internal/util/compare"
	"strconv"

	"appliers/api-applicant/pkg/constant"
	res "appliers/api-applicant/pkg/util/response"

	"github.com/labstack/echo/v4"
)

type ApplicantController struct {
	ApplicantService service.ApplicantService
}

func NewApplicantController(f *factory.ApplicantFactory) *ApplicantController {
	return &ApplicantController{
		ApplicantService: service.NewService(f),
	}
}

func (controller *ApplicantController) Create(c echo.Context) error {
	payload := new(dto.ApplicantRequestBody)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	applicant, err := controller.ApplicantService.Create(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(applicant).Send(c)
}

func (controller *ApplicantController) Update(c echo.Context) error {
	payload := new(dto.UpdateApplicantRequestBody)
	err := c.Bind(payload)
	id, errId := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil || errId != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	// Pass if the user realy have this data
	if !compare.CompareApplicantID(c.Request().Context(), uint(id)) {
		return res.ErrorResponse(&res.ErrorConstant.Unauthorized)
	}

	applicant, err := controller.ApplicantService.Update(c.Request().Context(), payload, uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(applicant).Send(c)
}

func (controller *ApplicantController) Delete(c echo.Context) error {
	id, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}

	err = controller.ApplicantService.Delete(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(nil).Send(c)
}

func (controller *ApplicantController) FindById(c echo.Context) error {
	var id, err = strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	applicant, err := controller.ApplicantService.FindById(c.Request().Context(), uint(id))
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(applicant).Send(c)
}

func (controller *ApplicantController) Find(c echo.Context) error {
	payload := new(dto.SearchGetRequest)
	if err := c.Bind(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	if err := c.Validate(payload); err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.Validation, err).Send(c)
	}

	result, err := controller.ApplicantService.Find(c.Request().Context(), payload)
	if err != nil {
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(result.Data).Send(c)
}

func (controller *ApplicantController) FindByUserId(c echo.Context) error {
	var userId, err = strconv.ParseUint(c.Param("user_id"), 10, 32)
	if err != nil {
		return res.ErrorBuilder(&res.ErrorConstant.BadRequest, err).Send(c)
	}
	applicant, err := controller.ApplicantService.FindByUserId(c.Request().Context(), uint(userId))
	if err != nil {
		if err == constant.RecordNotFound {
			res.ErrorBuilder(&res.ErrorConstant.NotFound, err).Send(c)
		}
		return res.ErrorResponse(err).Send(c)
	}
	return res.SuccessResponse(applicant).Send(c)
}
