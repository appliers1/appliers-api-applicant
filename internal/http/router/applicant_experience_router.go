package router

import (
	"appliers/api-applicant/internal/http/controller"
	"appliers/api-applicant/internal/http/middleware"
	. "appliers/api-applicant/pkg/constant"

	"github.com/labstack/echo/v4"
)

func ApplicantExperienceRouter(e *echo.Echo, c *controller.ApplicantExperienceController) {
	e.GET("/applicants/:id_applicant/experiences", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.POST("/applicants/:id_applicant/experiences", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.GET("/applicants/:id_applicant/experiences/:id_experience", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.PUT("/applicants/:id_applicant/experiences/:id_experience", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.DELETE("/applicants/:id_applicant/experiences/:id_experience", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)

}
