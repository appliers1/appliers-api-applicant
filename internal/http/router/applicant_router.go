package router

import (
	"appliers/api-applicant/internal/http/controller"
	"appliers/api-applicant/internal/http/middleware"
	. "appliers/api-applicant/pkg/constant"

	"github.com/labstack/echo/v4"
)

func ApplicantRouter(e *echo.Echo, c *controller.ApplicantController) {
	e.GET("/applicants", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true, ROLE_COMPANY: true}).Authentication)
	e.POST("/applicants", c.Create, middleware.NewAllowedRole(EmptyRole).Authentication)

	// dibawah akan di atur authorization nya lagi di service,
	// memastikan applicant terkait hanya bisa mengakses data nya sendiri
	e.GET("/applicants/:id", c.FindById, middleware.NewAllowedRole(AllRole).Authentication)
	e.PATCH("/applicants/:id", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	// end

	e.DELETE("/applicants/:id", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_ADMIN: true}).Authentication)
	e.GET("/applicants/users/:user_id", c.FindByUserId, middleware.NewAllowedRole(EmptyRole).Authentication)
}
