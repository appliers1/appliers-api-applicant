package router

import (
	"appliers/api-applicant/internal/http/controller"
	"appliers/api-applicant/internal/http/middleware"
	. "appliers/api-applicant/pkg/constant"

	"github.com/labstack/echo/v4"
)

func ApplicantEducationRouter(e *echo.Echo, c *controller.ApplicantEducationController) {
	e.GET("/applicants/:id_applicant/educations", c.Find, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.POST("/applicants/:id_applicant/educations", c.Create, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.GET("/applicants/:id_applicant/educations/:id_education", c.FindById, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.PUT("/applicants/:id_applicant/educations/:id_education", c.Update, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)
	e.DELETE("/applicants/:id_applicant/educations/:id_education", c.Delete, middleware.NewAllowedRole(map[UserRole]bool{ROLE_APPLICANT: true}).Authentication)

}
