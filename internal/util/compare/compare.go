package compare

import (
	"appliers/api-applicant/internal/util/jwtpayload"
	"appliers/api-applicant/pkg/constant"
	"context"
)

func CompareApplicantID(ctx context.Context, applicantId uint) bool {
	// get jwt payload
	cc := ctx.Value(constant.CONTEXT_JWT_PAYLOAD_KEY).(*jwtpayload.JWTPayload)
	// Pass if user's company equal company data value
	if cc.Role != constant.ROLE_APPLICANT || uint(cc.PersonId) != applicantId {
		return false
	}
	return true
}
