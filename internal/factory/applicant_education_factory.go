package factory

import (
	"appliers/api-applicant/internal/repository"

	"gorm.io/gorm"
)

type ApplicantEducationFactory struct {
	ApplicantRepository repository.ApplicantRepository
	EducationRepository repository.EducationRepository
}

func NewEducationFactory(db *gorm.DB) *ApplicantEducationFactory {
	return &ApplicantEducationFactory{
		ApplicantRepository: repository.NewApplicantRepositoryImplSql(db),
		EducationRepository: repository.NewEducationRepositoryImplSql(db),
	}
}
