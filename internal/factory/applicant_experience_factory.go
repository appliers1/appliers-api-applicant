package factory

import (
	"appliers/api-applicant/internal/repository"

	"gorm.io/gorm"
)

type ApplicantExperienceFactory struct {
	ApplicantRepository  repository.ApplicantRepository
	ExperienceRepository repository.ExperienceRepository
}

func NewApplicantExperienceFactory(db *gorm.DB) *ApplicantExperienceFactory {
	return &ApplicantExperienceFactory{
		ApplicantRepository:  repository.NewApplicantRepositoryImplSql(db),
		ExperienceRepository: repository.NewExperienceRepositoryImplSql(db),
	}
}
