package factory

import (
	"appliers/api-applicant/internal/repository"

	"gorm.io/gorm"
)

type ApplicantFactory struct {
	ApplicantRepository  repository.ApplicantRepository
	ExperienceRepository repository.ExperienceRepository
	EducationRepository  repository.EducationRepository
}

func NewApplicantFactory(db *gorm.DB) *ApplicantFactory {
	return &ApplicantFactory{
		ApplicantRepository:  repository.NewApplicantRepositoryImplSql(db),
		ExperienceRepository: repository.NewExperienceRepositoryImplSql(db),
		EducationRepository:  repository.NewEducationRepositoryImplSql(db),
	}
}
