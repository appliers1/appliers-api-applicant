package main

import (
	"appliers/api-applicant/database"
	"appliers/api-applicant/database/migration"
	"appliers/api-applicant/internal/http"
	"appliers/api-applicant/internal/http/middleware"
	"flag"
	"os"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
)

// load env configuration
func init() {
	if err := godotenv.Load(); err != nil {
		panic(err)
	}
	os.Setenv("GCP_CREDENTIALS", "appliers-355711-16cb26463718.json") // FILL IN WITH YOUR FILE PATH

}

func main() {

	database.CreateConnection()
	var m string // for check migration

	flag.StringVar(
		&m,
		"migrate",
		"run",
		`this argument for check if user want to migrate table, rollback table, or status migration

to use this flag:
	use -migrate=migrate for migrate table
	use -migrate=rollback for rollback table
	use -migrate=status for get status migration`,
	)
	flag.Parse()

	if m == "migrate" {
		migration.Migrate()
		return
	} else if m == "rollback" {
		migration.Rollback()
		return
	} else if m == "status" {
		migration.Status()
		return
	}
	db := database.GetConnection()
	e := echo.New()

	middleware.Init(e)
	http.Init(e, db)

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
